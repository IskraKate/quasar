export const selectAll = {
  bind(el){
    let input = el.querySelector('.q-field__native');
    if(input.value.length){
      input.addEventListener('focus', () => {
        input.select();
      });
    }
  }
}
